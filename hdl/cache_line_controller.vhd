-------------------------------------------------------------------------------
-- Module Name: cache_line_controller - Behavioral
-------------------------------------------------------------------------------
-- Copyright (c) 2022 ETH Zurich.
-- All rights reserved.
--
-- This file is distributed under the terms in the attached LICENSE file.
-- If you do not find this file, copies can be found by writing to:
-- ETH Zurich D-INFK, Stampfenbachstrasse 114, CH-8092 Zurich. Attn: Systems Group
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library UNISIM;
use UNISIM.vcomponents.all;

library xpm;
use xpm.vcomponents.all;

use work.eci_defs.all;

-- simple single cache line controller

entity cache_line_controller is
generic (
    ADDRESS             : std_logic_vector(39 downto 0)
);
port (
    clk : in std_logic;
    rst : in std_logic;

    eci_req         : in ECI_CHANNEL;
    eci_req_ready   : out std_logic;

    eci_rsp         : buffer ECI_CHANNEL;
    eci_rsp_ready   : in  std_logic;

    wr_data         : in WORDS(15 downto 0);
    wr_en           : in std_logic; -- local write request
    wr_done         : buffer std_logic := '0';

    rd_en           : in std_logic; -- local read request
    rd_valid        : buffer std_logic := '0';
    rd_data         : buffer WORDS(15 downto 0)
);
end cache_line_controller;

architecture Behavioral of cache_line_controller is

type state_type is (I, E, V, D, F);

-- States:
-- I - remote invalid, FPGA holds the cache line, local value is valid
-- E - remote exclusive, CPU holds the cache line, local value is invalid
-- V - remote exclusive, CPU asked to give the cache line back, local value is invalid
-- D - remote exclusive, CPU will hold the cache line, but it has to give it back first, local value is invalid
-- F - remote invalid, FPGA holds the cache line, but after access send it to the CPU, local value is valid

signal state            : state_type := I;
constant addr           : std_logic_vector(32 downto 0) := eci_alias_cache_line_index(ADDRESS(39 downto 7));
signal data             : WORDS(15 downto 0) := (others => (others => '0'));
signal got_rldd, got_rldx, got_vicd, got_vicdhi, got_haki : boolean;
signal cli              : std_logic_vector(32 downto 0);
signal opcode           : std_logic_vector(4 downto 0);
signal no_wr, no_rd     : std_logic;
signal eci_req_ready_b  : std_logic := '0';
signal eci_rsp_valid    : std_logic := '0';

begin

cli <= eci_req.data(0)(39 downto 7);
opcode <= eci_req.data(0)(63 downto 59);

no_wr <= '1' when wr_en = '0' or (wr_en = '1' and wr_done = '1') else '0';
no_rd <= '1' when rd_en = '0' or (rd_en = '1' and rd_valid = '1') else '0';
eci_rsp.valid <= eci_rsp_valid;

got_rldd <= (((state = I or state = E) and no_wr = '1' and no_rd = '1') or state = V) and eci_req.valid = '1' and eci_req.vc_no(3 downto 1) = "011" and opcode = ECI_MREQ_RLDD and cli = addr and eci_req_ready_b = '0' and eci_rsp_valid = '0';
got_rldx <= (((state = I or state = E) and no_wr = '1' and no_rd = '1') or state = V) and eci_req.valid = '1' and eci_req.vc_no(3 downto 1) = "011" and opcode = ECI_MREQ_RLDX and cli = addr and eci_req_ready_b = '0' and eci_rsp_valid = '0';
got_vicd <= (state = E or state = V or state = D) and eci_req.valid = '1' and (eci_req.vc_no(3 downto 1) = "101" or eci_req.vc_no(3 downto 1) = "010") and opcode = ECI_MRSP_VICD and cli = addr and eci_req_ready_b = '0';
got_vicdhi <= (state = V or state = D) and eci_req.valid = '1' and (eci_req.vc_no(3 downto 1) = "101" or eci_req.vc_no(3 downto 1) = "010") and opcode = ECI_MRSP_VICDHI and cli = addr and eci_req_ready_b = '0';
got_haki <= eci_req.valid = '1' and eci_req.vc_no(3 downto 1) = "101" and opcode = ECI_MRSP_HAKI and cli = addr and eci_req_ready_b = '0';
eci_req_ready <=  eci_req_ready_b;

rd_data <= data;

i_process : process(clk)
    variable rreqid : std_logic_vector(4 downto 0);
begin
    if rising_edge(clk) then
        if got_rldd or got_rldx then -- ECI_MREQ_RLDD/RLDX
            rreqid := eci_req.data(0)(54 downto 50);
            eci_rsp.data(0) <= ECI_MRSP_PEMD & "0000" & rreqid & "1111" & "1" & "00000" & addr & "0000000"; -- prepare PEMD
            eci_rsp.size <= ECI_CHANNEL_SIZE_8F;
            eci_rsp.vc_no <= "010" & not addr(0); -- 4 or 5
            if state = I then
                eci_rsp.data(8 downto 1) <= data(7 downto 0);
                eci_rsp_valid <= '1'; -- send PEMD
                rd_valid <= '0';
                wr_done <= '0';
                state <= E;
            else
                state <= D;
            end if;
            eci_req_ready_b <= '1';
        elsif got_vicd or got_vicdhi then -- ECI_MRSP_VICD, ECI_MRSP_VICDHI
            if eci_req.size = ECI_CHANNEL_SIZE_8F then
                data(7 downto 0) <= eci_req.data(8 downto 1);
            else -- ECI_CHANNEL_SIZE_17_2 or ECI_CHANNEL_SIZE_1
                if eci_req.size = ECI_CHANNEL_SIZE_8L then
                    data(15 downto 8) <= eci_req.data(8 downto 1);
                end if;
                if state = D then
                    state <= F;
                    rd_valid <= '1';
                else
                    state <= I;
                    rd_valid <= '1';
                end if;
            end if;
            eci_req_ready_b <= '1';
        elsif got_haki then -- ECI_MRSP_HAKI
            eci_req_ready_b <= '1';
        elsif state = E and (rd_en = '1' or wr_en = '1') and eci_rsp_valid = '0' then
            eci_rsp.data(0) <= ECI_MFWD_FEVX_EH & "000000000" & "1111" & "1" & "00100" & addr & "0000000"; -- FEVX_E
            eci_rsp.size <= ECI_CHANNEL_SIZE_0L;
            eci_rsp.vc_no <= "100" & not addr(0); -- 8 or 9
            eci_rsp_valid <= '1';
            state <= V;
        elsif state = I or state = F then
            if state = F and (wr_en = '0' or wr_done = '1') and (rd_en = '0' or rd_valid = '1') and eci_rsp_valid = '0' then
                eci_rsp.data(8 downto 1) <= data(7 downto 0);
                eci_rsp_valid <= '1'; -- send previously prepared PEMD
                rd_valid <= '0';
                wr_done <= '0';
                state <= E;
            elsif rd_en = '1' then
                rd_valid <= '1';
            elsif wr_en = '1' then
                data <= wr_data;
                wr_done <= '1';
            end if;
        end if;

        if eci_req_ready_b = '1' then
            eci_req_ready_b <= '0';
        end if;

        if eci_rsp_valid = '1' and eci_rsp_ready = '1' then
            if eci_rsp.size = ECI_CHANNEL_SIZE_8F then
                eci_rsp.data(8 downto 1) <= data(15 downto 8);
                eci_rsp.size <= ECI_CHANNEL_SIZE_8L;
            else
                eci_rsp_valid <= '0';
            end if;
        end if;
    end if;
end process;

end Behavioral;
