-------------------------------------------------------------------------------
-- Copyright (c) 2022 ETH Zurich.
-- All rights reserved.
--
-- This file is distributed under the terms in the attached LICENSE file.
-- If you do not find this file, copies can be found by writing to:
-- ETH Zurich D-INFK, Stampfenbachstrasse 114, CH-8092 Zurich. Attn: Systems Group
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library UNISIM;
use UNISIM.vcomponents.all;

library xpm;
use xpm.vcomponents.all;

use work.eci_defs.all;

entity memory_loopback is
port (
    clk : in std_logic;

    eci_req         : in ECI_CHANNEL;
    eci_req_ready   : buffer std_logic;
    eci_rsp         : buffer ECI_CHANNEL;
    eci_rsp_ready   : in  std_logic
);
end memory_loopback;

architecture behavioural of memory_loopback is

component ila_loopback is
port (
    clk : in std_logic;
    probe0 : in std_logic_vector(63 downto 0);
    probe1 : in std_logic_vector(2 downto 0);
    probe2 : in std_logic_vector(3 downto 0);
    probe3 : in std_logic_vector(0 downto 0);
    probe4 : in std_logic_vector(0 downto 0);
    probe5 : in std_logic_vector(63 downto 0);
    probe6 : in std_logic_vector(2 downto 0);
    probe7 : in std_logic_vector(3 downto 0);
    probe8 : in std_logic_vector(0 downto 0);
    probe9 : in std_logic_vector(0 downto 0);
    probe10 : in std_logic_vector(0 downto 0);
    probe11 : in std_logic_vector(0 downto 0)
);
end component;

-- Incoming read requests in the clk_eci domain
signal req_line     : std_logic_vector(32 downto 0);
signal req_id       : std_logic_vector(4 downto 0);
signal req_accepted : std_logic;
signal rstp_accepted : std_logic;

signal payload : std_logic_vector(1023 downto 0);
signal rsp_phase                : std_logic := '0';
signal req_data : std_logic;

begin

req_line <= eci_message_get_cli(eci_req.data(0));
req_id <= eci_message_get_request_id(eci_req.data(0));
req_accepted <= to_std_logic(eci_req.vc_no(3 downto 1) = "011" and (
    eci_message_get_command(eci_req.data(0)) = ECI_MREQ_RLDD or
    eci_message_get_command(eci_req.data(0)) = ECI_MREQ_RLDX or
    eci_message_get_command(eci_req.data(0)) = ECI_MREQ_RLDT or
    eci_message_get_command(eci_req.data(0)) = ECI_MREQ_RLDY));
rstp_accepted <= to_std_logic(eci_req.vc_no(3 downto 1) = "001" and is_eci_channel_cycle_last(eci_req.size) and (
    eci_message_get_command(eci_req.data(0)) = ECI_MREQ_RSTT or
    eci_message_get_command(eci_req.data(0)) = ECI_MREQ_RSTP or
    eci_message_get_command(eci_req.data(0)) = ECI_MREQ_RSTY));
eci_req_ready <= (eci_rsp_ready and req_accepted and rsp_phase) or (eci_rsp_ready and req_accepted and not rsp_phase and not req_data) or
    ((not req_accepted) and (not rstp_accepted or eci_rsp_ready));
req_data <= eci_req.data(0)(49) or eci_req.data(0)(48) or eci_req.data(0)(47) or eci_req.data(0)(46);
eci_rsp.data(8 downto 1) <= vector_to_words(payload(511 downto 0)) when rsp_phase = '0' else vector_to_words(payload(1023 downto 512));
eci_rsp.data(0) <=
    eci_mrsp_message(ECI_MRSP_PEMD, '0', req_id, "1111", '0', "0000", req_line, "00") when req_accepted = '1' and eci_message_get_dmask(eci_req.data(0)) /= "0000" else
    eci_mrsp_message(ECI_MRSP_PEMD, '0', req_id, "0000", '0', "0000", req_line, "00");
    
eci_rsp.size <= ECI_CHANNEL_SIZE_0L when rstp_accepted = '1' or eci_message_get_dmask(eci_req.data(0)) = "0000" else ECI_CHANNEL_SIZE_8F when rsp_phase = '0' else ECI_CHANNEL_SIZE_8L;
eci_rsp.vc_no(3 downto 1) <= "101" when eci_message_get_dmask(eci_req.data(0)) = "0000" else "010";
eci_rsp.vc_no(0) <= not eci_rsp.data(0)(7);
eci_rsp.valid <= eci_req.valid and (req_accepted or rstp_accepted);

i_process : process(clk)
begin
    if rising_edge(clk) then
        if eci_rsp.valid = '1' and eci_rsp_ready = '1' then
            if req_data = '1' and rsp_phase = '0' then
                rsp_phase <= '1';
            else
                rsp_phase <= '0';
            end if;
        end if;
    end if;
end process;

end behavioural;
