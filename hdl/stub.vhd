-------------------------------------------------------------------------------
-- Module Name: Enzian App - stub - Behavioral
-------------------------------------------------------------------------------
-- Copyright (c) 2022 ETH Zurich.
-- All rights reserved.
--
-- This file is distributed under the terms in the attached LICENSE file.
-- If you do not find this file, copies can be found by writing to:
-- ETH Zurich D-INFK, Stampfenbachstrasse 114, CH-8092 Zurich. Attn: Systems Group
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library UNISIM;
use UNISIM.vcomponents.all;

library xpm;
use xpm.vcomponents.all;

use work.eci_defs.all;

entity stub is
port (
-- 322.265625 MHz
    clk_sys                 : in std_logic;
-- programmed to 100MHz
    prgc0_clk_p             : in std_logic;
    prgc0_clk_n             : in std_logic;
-- programmed to 300MHz
    prgc1_clk_p             : in std_logic;
    prgc1_clk_n             : in std_logic;
-- power on reset
    reset_sys               : in std_logic;
-- ECI link status
    link1_up                : in std_logic;
    link2_up                : in std_logic;
-- ECI links
    link1_in_data           : in std_logic_vector(447 downto 0);
    link1_in_vc_no          : in std_logic_vector(27 downto 0);
    link1_in_we2            : in std_logic_vector(6 downto 0);
    link1_in_we3            : in std_logic_vector(6 downto 0);
    link1_in_we4            : in std_logic_vector(6 downto 0);
    link1_in_we5            : in std_logic_vector(6 downto 0);
    link1_in_valid          : in std_logic;
    link1_in_credit_return  : out std_logic_vector(12 downto 2);

    link1_out_hi_data       : out std_logic_vector(575 downto 0);
    link1_out_hi_vc_no      : out std_logic_vector(3 downto 0);
    link1_out_hi_size       : out std_logic_vector(2 downto 0);
    link1_out_hi_valid      : out std_logic;
    link1_out_hi_ready      : in std_logic;

    link1_out_lo_data       : out std_logic_vector(63 downto 0);
    link1_out_lo_vc_no      : out std_logic_vector(3 downto 0);
    link1_out_lo_valid      : out std_logic;
    link1_out_lo_ready      : in std_logic;
    link1_out_credit_return : in std_logic_vector(12 downto 2);

    link2_in_data           : in std_logic_vector(447 downto 0);
    link2_in_vc_no          : in std_logic_vector(27 downto 0);
    link2_in_we2            : in std_logic_vector(6 downto 0);
    link2_in_we3            : in std_logic_vector(6 downto 0);
    link2_in_we4            : in std_logic_vector(6 downto 0);
    link2_in_we5            : in std_logic_vector(6 downto 0);
    link2_in_valid          : in std_logic;
    link2_in_credit_return  : out std_logic_vector(12 downto 2);

    link2_out_hi_data       : out std_logic_vector(575 downto 0);
    link2_out_hi_vc_no      : out std_logic_vector(3 downto 0);
    link2_out_hi_size       : out std_logic_vector(2 downto 0);
    link2_out_hi_valid      : out std_logic;
    link2_out_hi_ready      : in std_logic;

    link2_out_lo_data       : out std_logic_vector(63 downto 0);
    link2_out_lo_vc_no      : out std_logic_vector(3 downto 0);
    link2_out_lo_valid      : out std_logic;
    link2_out_lo_ready      : in std_logic;
    link2_out_credit_return : in std_logic_vector(12 downto 2);
    
    disable_2nd_link        : out std_logic;
-- AXI Lite FPGA -> CPU
    m_io_axil_awaddr        : out std_logic_vector(43 downto 0);
    m_io_axil_awvalid       : out std_logic;
    m_io_axil_awready       : in std_logic;
    m_io_axil_wdata         : out std_logic_vector(63 downto 0);
    m_io_axil_wstrb         : out std_logic_vector(7 downto 0);
    m_io_axil_wvalid        : out std_logic;
    m_io_axil_wready        : in std_logic;
    m_io_axil_bresp         : in std_logic_vector(1 downto 0);
    m_io_axil_bvalid        : in std_logic;
    m_io_axil_bready        : out std_logic;
    m_io_axil_araddr        : out std_logic_vector(43 downto 0);
    m_io_axil_arvalid       : out std_logic;
    m_io_axil_arready       : in  std_logic;
    m_io_axil_rdata         : in std_logic_vector(63 downto 0);
    m_io_axil_rresp         : in std_logic_vector(1 downto 0);
    m_io_axil_rvalid        : in std_logic;
    m_io_axil_rready        : out std_logic;
-- AXI Lite CPU -> FPGA
    s_io_axil_awaddr        : in std_logic_vector(43 downto 0);
    s_io_axil_awvalid       : in std_logic;
    s_io_axil_awready       : out std_logic;
    s_io_axil_wdata         : in std_logic_vector(63 downto 0);
    s_io_axil_wstrb         : in std_logic_vector(7 downto 0);
    s_io_axil_wvalid        : in std_logic;
    s_io_axil_wready        : out std_logic;
    s_io_axil_bresp         : out std_logic_vector(1 downto 0);
    s_io_axil_bvalid        : out std_logic;
    s_io_axil_bready        : in std_logic;
    s_io_axil_araddr        : in std_logic_vector(43 downto 0);
    s_io_axil_arvalid       : in std_logic;
    s_io_axil_arready       : out  std_logic;
    s_io_axil_rdata         : out std_logic_vector(63 downto 0);
    s_io_axil_rresp         : out std_logic_vector(1 downto 0);
    s_io_axil_rvalid        : out std_logic;
    s_io_axil_rready        : in std_logic;
-- BSCAN slave port for ILAs, VIOs, MIGs, MDMs etc.
    s_bscan_bscanid_en      : in std_logic;
    s_bscan_capture         : in std_logic;
    s_bscan_drck            : in std_logic;
    s_bscan_reset           : in std_logic;
    s_bscan_runtest         : in std_logic;
    s_bscan_sel             : in std_logic;
    s_bscan_shift           : in std_logic;
    s_bscan_tck             : in std_logic;
    s_bscan_tdi             : in std_logic;
    s_bscan_tdo             : out std_logic;
    s_bscan_tms             : in std_logic;
    s_bscan_update          : in std_logic;
-- Microblaze Debug Module port
    mdm_SYS_Rst             : in std_logic;
    mdm_Clk                 : in std_logic;
    mdm_TDI                 : in std_logic;
    mdm_TDO                 : out std_logic;
    mdm_Reg_En              : in std_logic_vector(0 to 7);
    mdm_Capture             : in std_logic;
    mdm_Shift               : in std_logic;
    mdm_Update              : in std_logic;
    mdm_Rst                 : in std_logic;
    mdm_Disable             : in std_logic;
-- general purpose registers, accessible through the I/O space
    gpo_reg0            : in std_logic_vector(63 downto 0);
    gpo_reg1            : in std_logic_vector(63 downto 0);
    gpo_reg2            : in std_logic_vector(63 downto 0);
    gpo_reg3            : in std_logic_vector(63 downto 0);
    gpo_reg4            : in std_logic_vector(63 downto 0);
    gpo_reg5            : in std_logic_vector(63 downto 0);
    gpo_reg6            : in std_logic_vector(63 downto 0);
    gpo_reg7            : in std_logic_vector(63 downto 0);
    gpo_reg8            : in std_logic_vector(63 downto 0);
    gpo_reg9            : in std_logic_vector(63 downto 0);
    gpo_reg10           : in std_logic_vector(63 downto 0);
    gpo_reg11           : in std_logic_vector(63 downto 0);
    gpo_reg12           : in std_logic_vector(63 downto 0);
    gpo_reg13           : in std_logic_vector(63 downto 0);
    gpo_reg14           : in std_logic_vector(63 downto 0);
    gpo_reg15           : in std_logic_vector(63 downto 0);
    gpi_reg0            : out std_logic_vector(63 downto 0);
    gpi_reg1            : out std_logic_vector(63 downto 0);
    gpi_reg2            : out std_logic_vector(63 downto 0);
    gpi_reg3            : out std_logic_vector(63 downto 0);
    gpi_reg4            : out std_logic_vector(63 downto 0);
    gpi_reg5            : out std_logic_vector(63 downto 0);
    gpi_reg6            : out std_logic_vector(63 downto 0);
    gpi_reg7            : out std_logic_vector(63 downto 0);
    gpi_reg8            : out std_logic_vector(63 downto 0);
    gpi_reg9            : out std_logic_vector(63 downto 0);
    gpi_reg10           : out std_logic_vector(63 downto 0);
    gpi_reg11           : out std_logic_vector(63 downto 0);
    gpi_reg12           : out std_logic_vector(63 downto 0);
    gpi_reg13           : out std_logic_vector(63 downto 0);
    gpi_reg14           : out std_logic_vector(63 downto 0);
    gpi_reg15           : out std_logic_vector(63 downto 0);
-- DDR4
    F_D1_ACT_N : out std_logic;
    F_D1_A : out std_logic_vector ( 17 downto 0 );
    F_D1_BA : out std_logic_vector ( 1 downto 0 );
    F_D1_BG : out std_logic_vector ( 1 downto 0 );
    F_D1_CK_N : out std_logic_vector ( 1 downto 0 );
    F_D1_CK_P : out std_logic_vector ( 1 downto 0 );
    F_D1_CKE : out std_logic_vector ( 1 downto 0 );
    F_D1_CS_N : out std_logic_vector ( 3 downto 0 );
    F_D1_DQ : inout std_logic_vector ( 71 downto 0 );
    F_D1_DQS_N : inout std_logic_vector ( 17 downto 0 );
    F_D1_DQS_P : inout std_logic_vector ( 17 downto 0 );
    F_D1_ODT : out std_logic_vector ( 1 downto 0 );
    F_D1_PARITY_N : out std_logic;
    F_D1_RESET_N : out std_logic;
    F_D1C_CLK_N : in std_logic;
    F_D1C_CLK_P : in std_logic;

    F_D2_ACT_N : out std_logic;
    F_D2_A : out std_logic_vector ( 17 downto 0 );
    F_D2_BA : out std_logic_vector ( 1 downto 0 );
    F_D2_BG : out std_logic_vector ( 1 downto 0 );
    F_D2_CK_N : out std_logic_vector ( 1 downto 0 );
    F_D2_CK_P : out std_logic_vector ( 1 downto 0 );
    F_D2_CKE : out std_logic_vector ( 1 downto 0 );
    F_D2_CS_N : out std_logic_vector ( 3 downto 0 );
    F_D2_DQ : inout std_logic_vector ( 71 downto 0 );
    F_D2_DQS_N : inout std_logic_vector ( 17 downto 0 );
    F_D2_DQS_P : inout std_logic_vector ( 17 downto 0 );
    F_D2_ODT : out std_logic_vector ( 1 downto 0 );
    F_D2_PARITY_N : out std_logic;
    F_D2_RESET_N : out std_logic;
    F_D2C_CLK_N : in std_logic;
    F_D2C_CLK_P : in std_logic;

    F_D3_ACT_N : out std_logic;
    F_D3_A : out std_logic_vector ( 17 downto 0 );
    F_D3_BA : out std_logic_vector ( 1 downto 0 );
    F_D3_BG : out std_logic_vector ( 1 downto 0 );
    F_D3_CK_N : out std_logic_vector ( 1 downto 0 );
    F_D3_CK_P : out std_logic_vector ( 1 downto 0 );
    F_D3_CKE : out std_logic_vector ( 1 downto 0 );
    F_D3_CS_N : out std_logic_vector ( 3 downto 0 );
    F_D3_DQ : inout std_logic_vector ( 71 downto 0 );
    F_D3_DQS_N : inout std_logic_vector ( 17 downto 0 );
    F_D3_DQS_P : inout std_logic_vector ( 17 downto 0 );
    F_D3_ODT : out std_logic_vector ( 1 downto 0 );
    F_D3_PARITY_N : out std_logic;
    F_D3_RESET_N : out std_logic;
    F_D3C_CLK_N : in std_logic;
    F_D3C_CLK_P : in std_logic;

    F_D4_ACT_N : out std_logic;
    F_D4_A : out std_logic_vector ( 17 downto 0 );
    F_D4_BA : out std_logic_vector ( 1 downto 0 );
    F_D4_BG : out std_logic_vector ( 1 downto 0 );
    F_D4_CK_N : out std_logic_vector ( 1 downto 0 );
    F_D4_CK_P : out std_logic_vector ( 1 downto 0 );
    F_D4_CKE : out std_logic_vector ( 1 downto 0 );
    F_D4_CS_N : out std_logic_vector ( 3 downto 0 );
    F_D4_DQ : inout std_logic_vector ( 71 downto 0 );
    F_D4_DQS_N : inout std_logic_vector ( 17 downto 0 );
    F_D4_DQS_P : inout std_logic_vector ( 17 downto 0 );
    F_D4_ODT : out std_logic_vector ( 1 downto 0 );
    F_D4_PARITY_N : out std_logic;
    F_D4_RESET_N : out std_logic;
    F_D4C_CLK_N : in std_logic;
    F_D4C_CLK_P : in std_logic;
-- CMAC
    F_MAC0C_CLK_P   : in std_logic;
    F_MAC0C_CLK_N   : in std_logic;
    F_MAC0_TX_P : out std_logic_vector(3 downto 0);
    F_MAC0_TX_N : out std_logic_vector(3 downto 0);
    F_MAC0_RX_P : in std_logic_vector(3 downto 0);
    F_MAC0_RX_N : in std_logic_vector(3 downto 0);

    F_MAC1C_CLK_P   : in std_logic;
    F_MAC1C_CLK_N   : in std_logic;
    F_MAC1_TX_P : out std_logic_vector(3 downto 0);
    F_MAC1_TX_N : out std_logic_vector(3 downto 0);
    F_MAC1_RX_P : in std_logic_vector(3 downto 0);
    F_MAC1_RX_N : in std_logic_vector(3 downto 0);

    F_MAC2C_CLK_P   : in std_logic;
    F_MAC2C_CLK_N   : in std_logic;
    F_MAC2_TX_P : out std_logic_vector(3 downto 0);
    F_MAC2_TX_N : out std_logic_vector(3 downto 0);
    F_MAC2_RX_P : in std_logic_vector(3 downto 0);
    F_MAC2_RX_N : in std_logic_vector(3 downto 0);

    F_MAC3C_CLK_P   : in std_logic;
    F_MAC3C_CLK_N   : in std_logic;
    F_MAC3_TX_P : out std_logic_vector(3 downto 0);
    F_MAC3_TX_N : out std_logic_vector(3 downto 0);
    F_MAC3_RX_P : in std_logic_vector(3 downto 0);
    F_MAC3_RX_N : in std_logic_vector(3 downto 0);
-- PCIE x16
    F_PCIE16C_CLK_P   : in std_logic;
    F_PCIE16C_CLK_N   : in std_logic;
    F_PCIE16_TX_P : out std_logic_vector(15 downto 0);
    F_PCIE16_TX_N : out std_logic_vector(15 downto 0);
    F_PCIE16_RX_P : in std_logic_vector(15 downto 0);
    F_PCIE16_RX_N : in std_logic_vector(15 downto 0);
-- NVMe
    F_NVMEC_CLK_P   : in std_logic;
    F_NVMEC_CLK_N   : in std_logic;
    F_NVME_TX_P : out std_logic_vector(3 downto 0);
    F_NVME_TX_N : out std_logic_vector(3 downto 0);
    F_NVME_RX_P : in std_logic_vector(3 downto 0);
    F_NVME_RX_N : in std_logic_vector(3 downto 0);
-- C2C
    B_C2CC_CLK_P    : in std_logic;
    B_C2CC_CLK_N    : in std_logic;
    B_C2C_TX_P      : in std_logic_vector(0 downto 0);
    B_C2C_TX_N      : in std_logic_vector(0 downto 0);
    B_C2C_RX_P      : out std_logic_vector(0 downto 0);
    B_C2C_RX_N      : out std_logic_vector(0 downto 0);
    B_C2C_NMI       : in std_logic;
-- I2C
    F_I2C0_SDA      : inout std_logic;
    F_I2C0_SCL      : inout std_logic;

    F_I2C1_SDA      : inout std_logic;
    F_I2C1_SCL      : inout std_logic;

    F_I2C2_SDA      : inout std_logic;
    F_I2C2_SCL      : inout std_logic;

    F_I2C3_SDA      : inout std_logic;
    F_I2C3_SCL      : inout std_logic;

    F_I2C4_SDA      : inout std_logic;
    F_I2C4_SCL      : inout std_logic;

    F_I2C5_SDA      : inout std_logic;
    F_I2C5_SCL      : inout std_logic;
    F_I2C5_RESET_N  : out std_logic;
    F_I2C5_INT_N    : in std_logic;
-- FUART
    B_FUART_TXD     : in std_logic;
    B_FUART_RXD     : out std_logic;
    B_FUART_RTS     : in std_logic;
    B_FUART_CTS     : out std_logic;
-- IRQ
    F_IRQ_IRQ0      : out std_logic;
    F_IRQ_IRQ1      : out std_logic;
    F_IRQ_IRQ2      : out std_logic;
    F_IRQ_IRQ3      : out std_logic
);
end stub;

architecture Behavioral of stub is

component eci_gateway is
generic (
    RX_CROSSBAR_TYPE    : string := "full";
    TX_NO_CHANNELS      : integer;
    RX_NO_CHANNELS      : integer;
    RX_FILTER_VC        : VC_BITFIELDS;
    RX_FILTER_TYPE_MASK : ECI_TYPE_MASKS;
    RX_FILTER_TYPE      : ECI_TYPE_MASKS;
    RX_FILTER_CLI_MASK  : CLI_ARRAY;
    RX_FILTER_CLI       : CLI_ARRAY
);
port (
    clk_sys                 : in std_logic;
    clk_io_out              : out std_logic;
    clk_prgc0_out           : out std_logic;
    clk_prgc1_out           : out std_logic;

    prgc0_clk_p             : in std_logic;
    prgc0_clk_n             : in std_logic;
    prgc1_clk_p             : in std_logic;
    prgc1_clk_n             : in std_logic;

    reset_sys               : in std_logic;
    reset_out               : out std_logic;
    reset_n_out             : out std_logic;
    link1_up                : in std_logic;
    link2_up                : in std_logic;

    link1_in_data           : in std_logic_vector(447 downto 0);
    link1_in_vc_no          : in std_logic_vector(27 downto 0);
    link1_in_we2            : in std_logic_vector(6 downto 0);
    link1_in_we3            : in std_logic_vector(6 downto 0);
    link1_in_we4            : in std_logic_vector(6 downto 0);
    link1_in_we5            : in std_logic_vector(6 downto 0);
    link1_in_valid          : in std_logic;
    link1_in_credit_return  : out std_logic_vector(12 downto 2);

    link1_out_hi_data       : out std_logic_vector(575 downto 0);
    link1_out_hi_vc_no      : out std_logic_vector(3 downto 0);
    link1_out_hi_size       : out std_logic_vector(2 downto 0);
    link1_out_hi_valid      : out std_logic;
    link1_out_hi_ready      : in std_logic;

    link1_out_lo_data       : out std_logic_vector(63 downto 0);
    link1_out_lo_vc_no      : out std_logic_vector(3 downto 0);
    link1_out_lo_valid      : out std_logic;
    link1_out_lo_ready      : in std_logic;
    link1_out_credit_return : in std_logic_vector(12 downto 2);

    link2_in_data           : in std_logic_vector(447 downto 0);
    link2_in_vc_no          : in std_logic_vector(27 downto 0);
    link2_in_we2            : in std_logic_vector(6 downto 0);
    link2_in_we3            : in std_logic_vector(6 downto 0);
    link2_in_we4            : in std_logic_vector(6 downto 0);
    link2_in_we5            : in std_logic_vector(6 downto 0);
    link2_in_valid          : in std_logic;
    link2_in_credit_return  : out std_logic_vector(12 downto 2);

    link2_out_hi_data       : out std_logic_vector(575 downto 0);
    link2_out_hi_vc_no      : out std_logic_vector(3 downto 0);
    link2_out_hi_size       : out std_logic_vector(2 downto 0);
    link2_out_hi_valid      : out std_logic;
    link2_out_hi_ready      : in std_logic;

    link2_out_lo_data       : out std_logic_vector(63 downto 0);
    link2_out_lo_vc_no      : out std_logic_vector(3 downto 0);
    link2_out_lo_valid      : out std_logic;
    link2_out_lo_ready      : in std_logic;
    link2_out_credit_return : in std_logic_vector(12 downto 2);

    s_bscan_bscanid_en      : in std_logic;
    s_bscan_capture         : in std_logic;
    s_bscan_drck            : in std_logic;
    s_bscan_reset           : in std_logic;
    s_bscan_runtest         : in std_logic;
    s_bscan_sel             : in std_logic;
    s_bscan_shift           : in std_logic;
    s_bscan_tck             : in std_logic;
    s_bscan_tdi             : in std_logic;
    s_bscan_tdo             : out std_logic;
    s_bscan_tms             : in std_logic;
    s_bscan_update          : in std_logic;

    m0_bscan_bscanid_en     : out std_logic;
    m0_bscan_capture        : out std_logic;
    m0_bscan_drck           : out std_logic;
    m0_bscan_reset          : out std_logic;
    m0_bscan_runtest        : out std_logic;
    m0_bscan_sel            : out std_logic;
    m0_bscan_shift          : out std_logic;
    m0_bscan_tck            : out std_logic;
    m0_bscan_tdi            : out std_logic;
    m0_bscan_tdo            : in std_logic;
    m0_bscan_tms            : out std_logic;
    m0_bscan_update         : out std_logic;

    rx_eci_channels         : out ARRAY_ECI_CHANNELS(RX_NO_CHANNELS-1 downto 0);
    rx_eci_channels_ready   : in std_logic_vector(RX_NO_CHANNELS-1 downto 0);

    tx_eci_channels         : in ARRAY_ECI_CHANNELS(TX_NO_CHANNELS-1 downto 0);
    tx_eci_channels_ready   : out std_logic_vector(TX_NO_CHANNELS-1 downto 0)
);
end component;

component loopback_vc_resp_nodata is
generic (
   WORD_WIDTH : integer;
   GSDN_GSYNC_FN : integer
);
port (
    clk, reset : in std_logic;

    -- ECI Request input stream
    vc_req_i       : in  std_logic_vector(63 downto 0);
    vc_req_valid_i : in  std_logic;
    vc_req_ready_o : out std_logic;

    -- ECI Response output stream
    vc_resp_o       : out std_logic_vector(63 downto 0);
    vc_resp_valid_o : out std_logic;
    vc_resp_ready_i : in  std_logic
);
end component;

component memory_loopback is
port (
    clk : in std_logic;

    eci_req         : in ECI_CHANNEL;
    eci_req_ready   : buffer std_logic;
    eci_rsp         : buffer ECI_CHANNEL;
    eci_rsp_ready   : in  std_logic
);
end component;

component cache_line_controller is
generic (
    ADDRESS             : std_logic_vector(39 downto 0)
);
port (
    clk : in std_logic;
    rst : in std_logic;

    eci_req         : in ECI_CHANNEL;
    eci_req_ready   : out std_logic;

    eci_rsp         : buffer ECI_CHANNEL;
    eci_rsp_ready   : in  std_logic;

    wr_data         : in WORDS(15 downto 0);
    wr_en           : in std_logic;
    wr_done         : buffer std_logic := '0';

    rd_en           : in std_logic;
    rd_valid        : buffer std_logic := '0';
    rd_data         : buffer WORDS(15 downto 0)
);
end component;

component axi_bram_ctrl_0 is
port (
    s_axi_aclk : in std_logic;
    s_axi_aresetn : in std_logic;
    s_axi_awaddr : in std_logic_vector(12 downto 0);
    s_axi_awlen : in std_logic_vector(7 downto 0);
    s_axi_awsize : in std_logic_vector(2 downto 0);
    s_axi_awburst : in std_logic_vector(1 downto 0);
    s_axi_awlock : in std_logic;
    s_axi_awcache : in std_logic_vector(3 downto 0);
    s_axi_awprot : in std_logic_vector(2 downto 0);
    s_axi_awvalid : in std_logic;
    s_axi_awready : out std_logic;
    s_axi_wdata : in std_logic_vector(63 downto 0);
    s_axi_wstrb : in std_logic_vector(7 downto 0);
    s_axi_wlast : in std_logic;
    s_axi_wvalid : in std_logic;
    s_axi_wready : out std_logic;
    s_axi_bresp : out std_logic_vector(1 downto 0);
    s_axi_bvalid : out std_logic;
    s_axi_bready : in std_logic;
    s_axi_araddr : in std_logic_vector(12 downto 0);
    s_axi_arlen : in std_logic_vector(7 downto 0);
    s_axi_arsize : in std_logic_vector(2 downto 0);
    s_axi_arburst : in std_logic_vector(1 downto 0);
    s_axi_arlock : in std_logic;
    s_axi_arcache : in std_logic_vector(3 downto 0);
    s_axi_arprot : in std_logic_vector(2 downto 0);
    s_axi_arvalid : in std_logic;
    s_axi_arready : out std_logic;
    s_axi_rdata : out std_logic_vector(63 downto 0);
    s_axi_rresp : out std_logic_vector(1 downto 0);
    s_axi_rlast : out std_logic;
    s_axi_rvalid : out std_logic;
    s_axi_rready : in std_logic
);
end component;

component eci_channel_ila is
generic (
    NO_CHANNELS     : integer := 1
);
port (
    clk             : in std_logic;
    channels        : in ARRAY_ECI_CHANNELS(NO_CHANNELS downto 1);
    channels_ready  : in std_logic_vector(NO_CHANNELS downto 1)
);
end component;

type ECI_PACKET_RX is record
    c_gsync                 : ECI_CHANNEL;
    c_gsync_ready           : std_logic;
    c_ginv                  : ECI_CHANNEL;
    c6_data_loopback1       : ECI_CHANNEL;
    c6_data_loopback_ready1 : std_logic;
    c6_data_loopback2       : ECI_CHANNEL;
    c6_data_loopback_ready2 : std_logic;
    c7_data_loopback1       : ECI_CHANNEL;
    c7_data_loopback_ready1 : std_logic;
    c7_data_loopback2       : ECI_CHANNEL;
    c7_data_loopback_ready2 : std_logic;
end record ECI_PACKET_RX;

type ECI_PACKET_TX is record
-- VC packets inputs, from the ThunderX
-- GSYNC packets to be looped back
    c_gsdn                  : ECI_CHANNEL;
    c_gsdn_ready            : std_logic;
-- Responses with data (i.e. read response), to the ThunderX
    c4_data_loopback1       : ECI_CHANNEL;
    c4_data_loopback_ready1 : std_logic;
    c4_data_loopback2       : ECI_CHANNEL;
    c4_data_loopback_ready2 : std_logic;
    c5_data_loopback1       : ECI_CHANNEL;
    c5_data_loopback_ready1 : std_logic;
    c5_data_loopback2       : ECI_CHANNEL;
    c5_data_loopback_ready2 : std_logic;
end record ECI_PACKET_TX;

signal link_eci_packet_rx : ECI_PACKET_RX;
signal link_eci_packet_tx : ECI_PACKET_TX;

signal clk, clk_io : std_logic;
signal reset : std_logic;
signal reset_n : std_logic;
signal link_up : std_logic;

type BSCAN is record
    bscanid_en     : std_logic;
    capture        : std_logic;
    drck           : std_logic;
    reset          : std_logic;
    runtest        : std_logic;
    sel            : std_logic;
    shift          : std_logic;
    tck            : std_logic;
    tdi            : std_logic;
    tdo            : std_logic;
    tms            : std_logic;
    update         : std_logic;
end record BSCAN;
signal m0_bscan : BSCAN;

signal l1_eci_req         : ECI_CHANNEL;
signal l1_eci_req_ready   : std_logic;
signal l1_eci_rsp         : ECI_CHANNEL;
signal l1_eci_rsp_ready   : std_logic;
signal l2_eci_req         : ECI_CHANNEL;
signal l2_eci_req_ready   : std_logic;
signal l2_eci_rsp         : ECI_CHANNEL;
signal l2_eci_rsp_ready   : std_logic;

signal l1_wr_data             :  WORDS(15 downto 0) := (others => (others => '0'));
signal l1_wr_en               :  std_logic;
signal l1_wr_done             :  std_logic;
signal l1_rd_en               :  std_logic := '1';
signal l1_rd_valid            :  std_logic;
signal l1_rd_data             :  WORDS(15 downto 0) := (others => (others => '0'));

signal l2_wr_data             :  WORDS(15 downto 0) := (others => (others => '1'));
signal l2_wr_en               :  std_logic := '0';
signal l2_wr_done             :  std_logic;
signal l2_rd_en               :  std_logic;
signal l2_rd_valid            :  std_logic;
signal l2_rd_data             :  WORDS(15 downto 0) := (others => (others => '0'));

begin

clk <= clk_sys;

-- Sample register routing
gpi_reg0 <= x"000000000000dead";
gpi_reg1 <= x"000000000000beaf";
gpi_reg2 <= gpo_reg2;
gpi_reg3 <= gpo_reg3;
gpi_reg4 <= gpo_reg4;
gpi_reg5 <= gpo_reg5;
gpi_reg6 <= gpo_reg6;
gpi_reg7 <= gpo_reg7;
gpi_reg8 <= gpo_reg8;
gpi_reg9 <= gpo_reg9;
gpi_reg10 <= gpo_reg10;
gpi_reg11 <= gpo_reg11;
gpi_reg12 <= gpo_reg12;
gpi_reg13 <= gpo_reg13;
gpi_reg14 <= gpo_reg14;
gpi_reg15 <= gpo_reg15;

i_eci_gateway : eci_gateway
generic map (
    -- Receive ECI messages and route them to a receiver, based on the VC number, the ECI message type and ECI message cache line index
    -- If a message matches multiple filters, it's routed to the first matching receiver
    -- type of the receiving crossbar
    --  "full" - all VC paths are separate and independent, but resource utilization and latency increases
    --  "lite" - VCs are aggregated into 3 groups: (2, 3, 4, 5), (6, 8, 10, 12) and (7, 9, 11). It reduces resource utilization and latency, but doesn't permit blocking operations
    RX_CROSSBAR_TYPE    => "lite",
    -- number of transmitting channels
    TX_NO_CHANNELS      => 7,
    -- number of receiving channels
    RX_NO_CHANNELS      => 8,
    -- 1. GSYNC messages
    -- 2. GINV messages
    -- 3. Memory requests to the 1st simple cache line
    -- 4. Memory requests to the 2nd simple cache line
    -- 5. Memory requests to the 1st half of even cache lines - loopback
    -- 6. Memory requests to the 2nd half of even cache lines - loopback
    -- 7. Memory requests to the 1st half of odd cache lines - loopback
    -- 8. Memory requests to the 2nd half of odd cache lines - loopback
    -- receiving channels VC bitmasks
    RX_FILTER_VC        => (ECI_FILTER_VC_MASK((6, 7)),
                            ECI_FILTER_VC_MASK((6, 7)),
                            ECI_FILTER_VC_MASK((2, 4, 6, 10)),
                            ECI_FILTER_VC_MASK((3, 5, 7, 11)),
                            ECI_FILTER_VC_MASK((3, 5, 7)),
                            ECI_FILTER_VC_MASK((3, 5, 7)),
                            ECI_FILTER_VC_MASK((2, 4, 6)),
                            ECI_FILTER_VC_MASK((2, 4, 6))),
    -- receiving channels type masks, defines which type bits matter
    RX_FILTER_TYPE_MASK => ("11111",
                            "11111",
                            ECI_FILTER_TYPE_UNUSED,
                            ECI_FILTER_TYPE_UNUSED,
                            ECI_FILTER_TYPE_UNUSED,
                            ECI_FILTER_TYPE_UNUSED,
                            ECI_FILTER_TYPE_UNUSED,
                            ECI_FILTER_TYPE_UNUSED),
    -- receiving channels types, used to compare with the masked ECI type
    RX_FILTER_TYPE      => (ECI_MREQ_GSYNC,
                            ECI_MREQ_GINV,
                            ECI_FILTER_TYPE_UNUSED,
                            ECI_FILTER_TYPE_UNUSED,
                            ECI_FILTER_TYPE_UNUSED,
                            ECI_FILTER_TYPE_UNUSED,
                            ECI_FILTER_TYPE_UNUSED,
                            ECI_FILTER_TYPE_UNUSED),
    RX_FILTER_CLI_MASK  => (ECI_FILTER_CLI_UNUSED,
                            ECI_FILTER_CLI_UNUSED,
                            (others => '1'),
                            (others => '1'),
                            std_logic_vector(to_unsigned(0, 31)) & "10",
                            std_logic_vector(to_unsigned(0, 31)) & "10",
                            std_logic_vector(to_unsigned(0, 31)) & "10",
                            std_logic_vector(to_unsigned(0, 31)) & "10"),
    RX_FILTER_CLI       => (ECI_FILTER_CLI_UNUSED,
                            ECI_FILTER_CLI_UNUSED,
                            "1" & x"00000001",
                            "1" & x"00000002",
                            std_logic_vector(to_unsigned(0, 31)) & "00",
                            std_logic_vector(to_unsigned(0, 31)) & "10",
                            std_logic_vector(to_unsigned(0, 31)) & "00",
                            std_logic_vector(to_unsigned(0, 31)) & "10")
)
port map (
    clk_sys                 => clk,
    clk_io_out              => clk_io,

    prgc0_clk_p             => prgc0_clk_p,
    prgc0_clk_n             => prgc0_clk_n,
    prgc1_clk_p             => prgc1_clk_p,
    prgc1_clk_n             => prgc1_clk_n,

    reset_sys               => reset_sys,
    reset_out               => reset,
    reset_n_out             => reset_n,
    link1_up                => link1_up,
    link2_up                => link2_up,

    link1_in_data           => link1_in_data,
    link1_in_vc_no          => link1_in_vc_no,
    link1_in_we2            => link1_in_we2,
    link1_in_we3            => link1_in_we3,
    link1_in_we4            => link1_in_we4,
    link1_in_we5            => link1_in_we5,
    link1_in_valid          => link1_in_valid,
    link1_in_credit_return  => link1_in_credit_return,

    link1_out_hi_data       => link1_out_hi_data,
    link1_out_hi_vc_no      => link1_out_hi_vc_no,
    link1_out_hi_size       => link1_out_hi_size,
    link1_out_hi_valid      => link1_out_hi_valid,
    link1_out_hi_ready      => link1_out_hi_ready,

    link1_out_lo_data       => link1_out_lo_data,
    link1_out_lo_vc_no      => link1_out_lo_vc_no,
    link1_out_lo_valid      => link1_out_lo_valid,
    link1_out_lo_ready      => link1_out_lo_ready,
    link1_out_credit_return => link1_out_credit_return,

    link2_in_data           => link2_in_data,
    link2_in_vc_no          => link2_in_vc_no,
    link2_in_we2            => link2_in_we2,
    link2_in_we3            => link2_in_we3,
    link2_in_we4            => link2_in_we4,
    link2_in_we5            => link2_in_we5,
    link2_in_valid          => link2_in_valid,
    link2_in_credit_return  => link2_in_credit_return,

    link2_out_hi_data       => link2_out_hi_data,
    link2_out_hi_vc_no      => link2_out_hi_vc_no,
    link2_out_hi_size       => link2_out_hi_size,
    link2_out_hi_valid      => link2_out_hi_valid,
    link2_out_hi_ready      => link2_out_hi_ready,

    link2_out_lo_data       => link2_out_lo_data,
    link2_out_lo_vc_no      => link2_out_lo_vc_no,
    link2_out_lo_valid      => link2_out_lo_valid,
    link2_out_lo_ready      => link2_out_lo_ready,
    link2_out_credit_return => link2_out_credit_return,

    s_bscan_bscanid_en      => s_bscan_bscanid_en,
    s_bscan_capture         => s_bscan_capture,
    s_bscan_drck            => s_bscan_drck,
    s_bscan_reset           => s_bscan_reset,
    s_bscan_runtest         => s_bscan_runtest,
    s_bscan_sel             => s_bscan_sel,
    s_bscan_shift           => s_bscan_shift,
    s_bscan_tck             => s_bscan_tck,
    s_bscan_tdi             => s_bscan_tdi,
    s_bscan_tdo             => s_bscan_tdo,
    s_bscan_tms             => s_bscan_tms,
    s_bscan_update          => s_bscan_update,

    m0_bscan_bscanid_en     => m0_bscan.bscanid_en,
    m0_bscan_capture        => m0_bscan.capture,
    m0_bscan_drck           => m0_bscan.drck,
    m0_bscan_reset          => m0_bscan.reset,
    m0_bscan_runtest        => m0_bscan.runtest,
    m0_bscan_sel            => m0_bscan.sel,
    m0_bscan_shift          => m0_bscan.shift,
    m0_bscan_tck            => m0_bscan.tck,
    m0_bscan_tdi            => m0_bscan.tdi,
    m0_bscan_tdo            => m0_bscan.tdo,
    m0_bscan_tms            => m0_bscan.tms,
    m0_bscan_update         => m0_bscan.update,

    rx_eci_channels(0)      => link_eci_packet_rx.c_gsync,
    rx_eci_channels(1)      => link_eci_packet_rx.c_ginv,
    rx_eci_channels(2)      => l1_eci_req,
    rx_eci_channels(3)      => l2_eci_req,
    rx_eci_channels(4)      => link_eci_packet_rx.c7_data_loopback1,
    rx_eci_channels(5)      => link_eci_packet_rx.c7_data_loopback2,
    rx_eci_channels(6)      => link_eci_packet_rx.c6_data_loopback1,
    rx_eci_channels(7)      => link_eci_packet_rx.c6_data_loopback2,

    rx_eci_channels_ready(0)   => link_eci_packet_rx.c_gsync_ready,
    rx_eci_channels_ready(1)   => '1',
    rx_eci_channels_ready(2)   => l1_eci_req_ready,
    rx_eci_channels_ready(3)   => l2_eci_req_ready,
    rx_eci_channels_ready(4)   => link_eci_packet_rx.c7_data_loopback_ready1,
    rx_eci_channels_ready(5)   => link_eci_packet_rx.c7_data_loopback_ready2,
    rx_eci_channels_ready(6)   => link_eci_packet_rx.c6_data_loopback_ready1,
    rx_eci_channels_ready(7)   => link_eci_packet_rx.c6_data_loopback_ready2,

    tx_eci_channels(0)      => link_eci_packet_tx.c_gsdn,
    tx_eci_channels(1)      => l1_eci_rsp,
    tx_eci_channels(2)      => l2_eci_rsp,
    tx_eci_channels(3)      => link_eci_packet_tx.c5_data_loopback1,
    tx_eci_channels(4)      => link_eci_packet_tx.c5_data_loopback2,
    tx_eci_channels(5)      => link_eci_packet_tx.c4_data_loopback1,
    tx_eci_channels(6)      => link_eci_packet_tx.c4_data_loopback2,

    tx_eci_channels_ready(0)   => link_eci_packet_tx.c_gsdn_ready,
    tx_eci_channels_ready(1)   => l1_eci_rsp_ready,
    tx_eci_channels_ready(2)   => l2_eci_rsp_ready,
    tx_eci_channels_ready(3)   => link_eci_packet_tx.c5_data_loopback_ready1,
    tx_eci_channels_ready(4)   => link_eci_packet_tx.c5_data_loopback_ready2,
    tx_eci_channels_ready(5)   => link_eci_packet_tx.c4_data_loopback_ready1,
    tx_eci_channels_ready(6)   => link_eci_packet_tx.c4_data_loopback_ready2
);

-- Send GSDN responses to GSYNC requests
gsync_loopback : loopback_vc_resp_nodata
generic map (
   WORD_WIDTH => 64,
   GSDN_GSYNC_FN => 1
)
port map (
    clk   => clk,
    reset => reset,

    vc_req_i       => link_eci_packet_rx.c_gsync.data(0),
    vc_req_valid_i => link_eci_packet_rx.c_gsync.valid,
    vc_req_ready_o => link_eci_packet_rx.c_gsync_ready,

    vc_resp_o       => link_eci_packet_tx.c_gsdn.data(0),
    vc_resp_valid_o => link_eci_packet_tx.c_gsdn.valid,
    vc_resp_ready_i => link_eci_packet_tx.c_gsdn_ready
);

link_eci_packet_tx.c_gsdn.vc_no(3 downto 1) <= "101";  -- VCs 10/11
link_eci_packet_tx.c_gsdn.vc_no(0) <= not link_eci_packet_tx.c_gsdn.data(0)(7); -- 10 or 11 depends on the lowest bit of the cache line index
link_eci_packet_tx.c_gsdn.size <= ECI_CHANNEL_SIZE_0L;

 -- Even cache lines
memory_even1 : memory_loopback
port map (
    clk => clk,

    -- Reads on VC7
    eci_req         => link_eci_packet_rx.c7_data_loopback1,
    eci_req_ready   => link_eci_packet_rx.c7_data_loopback_ready1,

    -- Responses on VC5
    eci_rsp         => link_eci_packet_tx.c5_data_loopback1,
    eci_rsp_ready   => link_eci_packet_tx.c5_data_loopback_ready1
);

memory_even2 : memory_loopback
port map (
    clk => clk,

    -- Reads on VC7
    eci_req         => link_eci_packet_rx.c7_data_loopback2,
    eci_req_ready   => link_eci_packet_rx.c7_data_loopback_ready2,

    -- Responses on VC5
    eci_rsp         => link_eci_packet_tx.c5_data_loopback2,
    eci_rsp_ready   => link_eci_packet_tx.c5_data_loopback_ready2
);

 -- Odd cache lines
memory_odd1 : memory_loopback
port map (
    clk => clk,

    -- Reads on VC6
    eci_req         => link_eci_packet_rx.c6_data_loopback1,
    eci_req_ready   => link_eci_packet_rx.c6_data_loopback_ready1,

    -- Responses on VC4
    eci_rsp         => link_eci_packet_tx.c4_data_loopback1,
    eci_rsp_ready   => link_eci_packet_tx.c4_data_loopback_ready1
);

memory_odd2 : memory_loopback
port map (
    clk => clk,

    -- Reads on VC6
    eci_req         => link_eci_packet_rx.c6_data_loopback2,
    eci_req_ready   => link_eci_packet_rx.c6_data_loopback_ready2,

    -- Responses on VC4
    eci_rsp         => link_eci_packet_tx.c4_data_loopback2,
    eci_rsp_ready   => link_eci_packet_tx.c4_data_loopback_ready2
);

clc_1 : cache_line_controller
generic map (
    ADDRESS             => x"8000000080"
)
port map (
    clk                 => clk,
    rst                 => '0',

    eci_req             => l1_eci_req,
    eci_req_ready       => l1_eci_req_ready,

    eci_rsp             => l1_eci_rsp,
    eci_rsp_ready       => l1_eci_rsp_ready,

    wr_data             => l1_wr_data,
    wr_en               => l1_wr_en,
    wr_done             => l1_wr_done,

    rd_en               => l1_rd_en,
    rd_valid            => l1_rd_valid,
    rd_data             => l1_rd_data
);

clc_2 : cache_line_controller
generic map (
    ADDRESS             => x"8000000100"
)
port map (
    clk                 => clk,
    rst                 => '0',

    eci_req             => l2_eci_req,
    eci_req_ready       => l2_eci_req_ready,

    eci_rsp             => l2_eci_rsp,
    eci_rsp_ready       => l2_eci_rsp_ready,

    wr_data             => l2_wr_data,
    wr_en               => l2_wr_en,
    wr_done             => l2_wr_done,

    rd_en               => l2_rd_en,
    rd_valid            => l2_rd_valid,
    rd_data             => l2_rd_data
);

-- copy the value from the cache line 1 to the cache line 2 when it gets updated
i_process : process(clk)
begin
    if rising_edge(clk) then
        if l1_rd_valid = '1' and l1_rd_data /= l2_wr_data and l2_wr_en = '0' then
            l2_wr_data <= l1_rd_data;
            l2_wr_en <= '1';
        end if;
        if l2_wr_en = '1' and l2_wr_done = '1' then
            l2_wr_en <= '0';
        end if;
    end if;
end process;

-- Instantiate a BRAM module that can be accessed from a CPU through the FPGA I/O space (0x9000'0000'0000-0x9fff'ffff'ffff)
i_axi_bram : axi_bram_ctrl_0
port map (
    s_axi_aclk  => clk,
    s_axi_aresetn   => reset_n,
    s_axi_awaddr    => s_io_axil_awaddr(12 downto 0),
    s_axi_awlen     => x"00",
    s_axi_awsize    => "000",
    s_axi_awburst   => "00",
    s_axi_awlock    => '0',
    s_axi_awcache   => "0000",
    s_axi_awprot    => "000",
    s_axi_awvalid   => s_io_axil_awvalid,
    s_axi_awready   => s_io_axil_awready,
    s_axi_wdata     => s_io_axil_wdata,
    s_axi_wstrb     => s_io_axil_wstrb,
    s_axi_wlast     => '1',
    s_axi_wvalid    => s_io_axil_wvalid,
    s_axi_wready    => s_io_axil_wready,
    s_axi_bresp     => s_io_axil_bresp,
    s_axi_bvalid    => s_io_axil_bvalid,
    s_axi_bready    => s_io_axil_bready,
    s_axi_araddr    => s_io_axil_araddr(12 downto 0),
    s_axi_arlen     => x"00",
    s_axi_arsize    => "000",
    s_axi_arburst   => "00",
    s_axi_arlock    => '0',
    s_axi_arcache   => "0000",
    s_axi_arprot    => "000",
    s_axi_arvalid   => s_io_axil_arvalid,
    s_axi_arready   => s_io_axil_arready,
    s_axi_rdata     => s_io_axil_rdata,
    s_axi_rresp     => s_io_axil_rresp,
    s_axi_rlast     => open,
    s_axi_rvalid    => s_io_axil_rvalid,
    s_axi_rready    => s_io_axil_rready
);

end Behavioral;
