# Enzian App Stub

An example Enzian application.
It responds to GSYNC messages, memory read and write requests with dummy data.
It provides two readable/writable cache lines (addresses 0x80'0000'0080 and 0x80'0000'0100) that are used by the cache-to-cache benchmark.
When new data is written to the 1st cache line, the data is copied to the 2nd cache line.
